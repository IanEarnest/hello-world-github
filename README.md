Hello-World-GitHub project
===========
BEING CHANGED
===========
This Hello World project is setup to show multiple examples of Hello World in programming, using GitHub and languages:
C#
Unity
C++
Java
Eclipse
HTML5


Intro
-------

This is a C# "Hello World" example created in Unity using MonoDevelop.

Main code located: /Hello World Unity/Assets/HelloWorldScript.cs


Purpose
-------

Print on screen "Hello World".



How to run
----------

Download the folder "Builds" in the directory "Hello World Unity" and then run "Hello World 1.exe" in the "Builds" folder.


